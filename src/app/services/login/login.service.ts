import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth, ResponseServer } from '../../interfaces';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private loginStatus: boolean;
  public _userData;
  constructor(private _http: HttpClient) {
    this.loginStatus = false;
  }
  getUser(user: Auth): Observable<ResponseServer> {
    return this._http.post<ResponseServer>(
      environment.apiPycloudmeDev + 'auth',
      user
    );
  }

  logged(value: boolean): void {
    sessionStorage.setItem('lS', JSON.stringify(value));
  }

  isLogged(): boolean {
    this.loginStatus = JSON.parse(sessionStorage.getItem('lS'));
    return this.loginStatus;
  }

  set userData(data) {
    sessionStorage.setItem('user_data', JSON.stringify(data));
    this._userData = data;
  }

  get userData() {
    this._userData = JSON.parse(sessionStorage.getItem('user_data'));
    return this._userData;
  }
}
