export interface User {
  id: number;
  rol_id: number;
  tpd_document_id: number;
  empresa_ips_id: number;
  usuario_id: number;
  nickname: string;
  nombres: string;
  apellido: string;
  number_document: number;
  celular: number;
  correo: number;
  direccion: string;
  fecha_nacimiento: string;
  telefono_fijo: string;
  actived: boolean;
  createat_at: string;
  updated_at: string;
  deleted_at: string;
}

export enum TypeDocument {
  TarjetaIdentidad,
  CedulaCiudadania,
  Pasaporte
}

export enum Rol {
  Default,
  Administrador,
  Gestor,
  Medico,
  Paciente
}

export interface Auth {
  tpd_document_id: number;
  number_document: string;
  nickname: string;
  password: string;
}

export interface ResponseServer {
  data: any;
  message: string;
}
