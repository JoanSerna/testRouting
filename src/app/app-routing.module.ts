import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {LoginGuard} from './guards/login/login.guard';
import {GestorComponent} from './gestor/gestor.component';
import {AuthGuard} from './guards/auth/auth.guard';
import {UsuarioComponent} from './shared/usuario/usuario.component';
import {AgendarComponent} from './gestor/agendar/agendar.component';
import {AyudaComponent} from './shared/ayuda/ayuda.component';
import {PacienteComponent} from './paciente/paciente.component';
import {AdministradorComponent} from './administrador/administrador.component';
import {MedicoComponent} from './medico/medico.component';

const routes: Routes = [
  {path: '', redirectTo: '/', pathMatch: 'full'},
  {path: '', component: LoginComponent, canActivate: [LoginGuard]},
  {
    path: 'gestor', component: GestorComponent, canActivate: [AuthGuard], children: [
      {path: '', redirectTo: '', pathMatch: 'full'},
      {path: 'usuario', component: UsuarioComponent},
      {path: 'agendar', component: AgendarComponent},
      {path: 'ayuda', component: AyudaComponent},
    ],
  },
  {path: 'paciente', component: PacienteComponent, canActivate: [AuthGuard]},
  {path: 'administrador', component: AdministradorComponent, canActivate: [AuthGuard]},
  {path: 'medico', component: MedicoComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
